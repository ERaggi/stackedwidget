#include "addfieldform.h"
#include "ui_addfieldform.h"
#include <QInputDialog>

AddFieldForm::AddFieldForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AddFieldForm)
{
    ui->setupUi(this);
    ui->comboBoxFieldForms->setCurrentIndex(0);
}
AddFieldForm::~AddFieldForm()
{
    delete ui;
}

void AddFieldForm::on_pushButton_clicked()
{

}

void AddFieldForm::on_comboBoxFieldForms_currentIndexChanged(const QString &arg1)
{
    QString list = ui->comboBoxFieldForms->currentText();
    ui->lineEdit->setText(list);
    Q_UNUSED(arg1)
}
