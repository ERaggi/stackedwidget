#include "vesselpossystemwidget.h"
#include "ui_vesselpossystemwidget.h"

VesselPosSystemWidget::VesselPosSystemWidget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::VesselPosSystemWidget)
{
    ui->setupUi(this);
}

VesselPosSystemWidget::~VesselPosSystemWidget()
{
    delete ui;
}

