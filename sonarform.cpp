#include "sonarform.h"
#include "ui_sonarform.h"

SonarForm::SonarForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SonarForm)
{
    ui->setupUi(this);
}

SonarForm::~SonarForm()
{
    delete ui;
}
