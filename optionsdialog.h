#ifndef OPTIONSDIALOG_H
#define OPTIONSDIALOG_H

#include <QDialog>
#include <QAction>
#include <QSqlTableModel>
#include <QComboBox>
#include "vesselpossystemwidget.h"
#include "sonarform.h"
#include "outputform.h"
#include "newtabledialog.h"
#include "editform.h"
#include "addfieldform.h"
#include "addfieldform.h"

#include <QComboBox>
#include <addfieldform.h>

//#include <QStackedWidget>
//#include <QListWidget>

namespace Ui {
class OptionsDialog;
}

class OptionsDialog : public QDialog
{
    Q_OBJECT
public:
    explicit OptionsDialog(QWidget *parent = nullptr);
    ~OptionsDialog();
    // disabled
    OptionsDialog(const OptionsDialog&) = delete;
    OptionsDialog& operator=(const OptionsDialog&) = delete;
    void openDialog();
    void setName(QString table);
    bool eventFilter(QObject *watched, QEvent *event);



public slots:
//    void changeWidget(const QString& changedText);
    void on_comboBox_currentRowChanged(QString itemChanged);

private slots:
    void on_listWidget_currentRowChanged(int currentRow);

    void on_pushButton_clicked();

    void on_tableView_doubleClicked(const QModelIndex &index);

    void on_addBtn_clicked();

private:
    Ui::OptionsDialog *ui;
    VesselPosSystemWidget *mVesPos;
    SonarForm *mSonar;
    OutputForm *mOutput;
//    QStackedWidget *stack;
//    QListWidget *list;
    NewTableDialog *mNewTable;
    AddFieldForm *mAddForm;
    QAction *mActionAddField;
    QAction *mActionDeleteField;
    QAction *mActionInfoLaserMsgs;
    QAction *mAddGuidedLaserTableProcedure;
    QAction *mAddMsgs;
    QAction *mDeleteMsgs;
    QAction *mAddMsgsViaInputDialog;

    QAction *addRow;
    QAction *eraseRow;
    QComboBox *mComboBox;

    QSqlTableModel *mModel;
    QString workingTable;

    EditForm *mEdit;
    AddFieldForm *mAddFieldForm;


};

#endif // OPTIONSDIALOG_H
