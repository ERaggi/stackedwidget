#ifndef VESSELPOSSYSTEMWIDGET_H
#define VESSELPOSSYSTEMWIDGET_H

#include <QWidget>

namespace Ui {
class VesselPosSystemWidget;
}


class VesselPosSystemWidget : public QWidget
{
    Q_OBJECT

public:
    VesselPosSystemWidget(QWidget *parent = nullptr);
    ~VesselPosSystemWidget();
    // disabled
    VesselPosSystemWidget(const VesselPosSystemWidget&) = delete;
    VesselPosSystemWidget& operator=(const VesselPosSystemWidget&) = delete;

private:
    Ui::VesselPosSystemWidget *ui;
};
#endif // VESSELPOSSYSTEMWIDGET_H
