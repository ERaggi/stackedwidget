#ifndef ADDFIELDFORM_H
#define ADDFIELDFORM_H

#include <QWidget>
namespace Ui {
class AddFieldForm;
}
class AddFieldForm : public QWidget
{
    Q_OBJECT
public:
    explicit AddFieldForm(QWidget *parent = nullptr);
    ~AddFieldForm();
private slots:
    void on_pushButton_clicked();

    void on_comboBoxFieldForms_currentIndexChanged(const QString &arg1);

private:
    Ui::AddFieldForm *ui;
};

#endif // ADDFIELDFORM_H
