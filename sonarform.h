#ifndef SONARFORM_H
#define SONARFORM_H

#include <QWidget>

namespace Ui {
class SonarForm;
}

class SonarForm : public QWidget
{
    Q_OBJECT

public:
    SonarForm(QWidget *parent = nullptr);
    ~SonarForm();
    // disabled
    SonarForm(const SonarForm&) = delete;
    SonarForm& operator=(const SonarForm&) = delete;

private:
    Ui::SonarForm *ui;
};

#endif // SONARFORM_H
