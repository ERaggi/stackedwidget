#include "optionsdialog.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    OptionsDialog w;
    w.show();
    return a.exec();
}
