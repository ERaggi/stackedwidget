#ifndef OUTPUTFORM_H
#define OUTPUTFORM_H

#include <QWidget>

namespace Ui {
class OutputForm;
}

class OutputForm : public QWidget
{
    Q_OBJECT

public:
    OutputForm(QWidget *parent = nullptr);
    ~OutputForm();
    // disabled
    OutputForm(const OutputForm&) = delete;
    OutputForm& operator=(const OutputForm&) = delete;

private:
    Ui::OutputForm *ui;
};

#endif // OUTPUTFORM_H
