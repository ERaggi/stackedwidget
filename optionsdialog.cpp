#include "optionsdialog.h"
#include "ui_optionsdialog.h"
#include <QDebug>
#include <QInputDialog>
#include <QCheckBox>
#include <QComboBox>

OptionsDialog::OptionsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::OptionsDialog)
{
    ui->setupUi(this);
    mVesPos = new VesselPosSystemWidget;
    mSonar = new SonarForm;
    mOutput = new OutputForm;
    ui->stackedWidget->setCurrentIndex(0);
    ui->stackedWidget->insertWidget(0, mVesPos);
    ui->stackedWidget->insertWidget(1, mSonar);
    ui->stackedWidget->insertWidget(2, mOutput);

    //    ui->horizontalLayout->addWidget(ui->stackedWidget);
//    setLayout(ui->horizontalLayout);


    connect(ui->tableView, SIGNAL(doubleClicked), this, SLOT(on_tableView_doubleClicked(const QModelIndex &index)));

    ui->tableWidget->setContextMenuPolicy(Qt::ActionsContextMenu);
    mActionAddField = new QAction(QIcon(":"), tr("Add"), this);
    mActionDeleteField = new QAction(QIcon(":"), tr("Remove"), this);
    ui->tableWidget->addActions({mActionAddField, mActionDeleteField});

    mComboBox = new QComboBox(parent);
    mComboBox->addItems({"INTEGER","DOUBLE","TEXT","DATE","TIME","BOOL","BLOB", "ADD FIELD"});

//    auto newComboBox = [&](QWidget *parent) {
//        auto combo = new QComboBox(parent);
//        combo->addItems({"INTEGER","DOUBLE","TEXT","DATE","TIME","BOOL","BLOB", "ADD FIELD"});
//        return combo;
//    };

    mAddForm = new AddFieldForm(parent);

    mComboBox = new QComboBox(parent);
    mComboBox->addItems({"INTEGER","DOUBLE","TEXT","DATE","TIME","BOOL","BLOB", "ADD FIELD"});

    connect(mComboBox,SIGNAL(currentIndexChanged(QString)),
            this,SLOT(on_comboBox_currentRowChanged(QString)));


    connect(mActionAddField, &QAction::triggered, [&]() {
        int rowCount = ui->tableWidget->rowCount();
        ui->tableWidget->insertRow(rowCount);
        ui->tableWidget->setCellWidget(rowCount, 1, mComboBox);
        ui->tableWidget->setCellWidget(rowCount, 2, new QRadioButton(this));
        ui->tableWidget->setCellWidget(rowCount, 3, new QCheckBox(this));
        ui->tableWidget->setCellWidget(rowCount, 4, new QCheckBox(this));
        ui->tableWidget->show();
    });
    connect(mActionDeleteField, &QAction::triggered, [&]() {
        ui->tableWidget->removeRow(ui->tableWidget->currentRow());
    });




    addRow = new QAction(QIcon(":add_item.png"), tr("Add Row"), this);
    eraseRow = new QAction(QIcon(":remove_item.png"), tr("Delete Row"), this);
    setWindowTitle("new table");
    mModel = new QSqlTableModel(this);
    mModel->setEditStrategy(QSqlTableModel::OnManualSubmit);
//    mModel->setTable("new table");
    mModel->select();
    ui->tableView->setModel(mModel);
    ui->tableView->setContextMenuPolicy(Qt::ActionsContextMenu);
    ui->tableView->addActions({addRow, eraseRow});

    connect(addRow, &QAction::triggered, [&]() {
        mModel->insertRow(mModel->rowCount());
        mModel->submitAll();
    });
    connect(eraseRow, &QAction::triggered, [&]() {
       mModel->removeRow(ui->tableView->currentIndex().row());
    });


    ui->tableView->viewport()->installEventFilter(this);



}

OptionsDialog::~OptionsDialog()
{
    delete ui;
}

void OptionsDialog::openDialog()
{
    QString ros = QInputDialog::getText(this, "Dialog", "Enter text");
}


void OptionsDialog::on_listWidget_currentRowChanged(int currentRow)
{
    ui->stackedWidget->setCurrentIndex(currentRow);
    qDebug() << currentRow;
}

void OptionsDialog::on_pushButton_clicked()
{
    mNewTable = new NewTableDialog();
    mNewTable->show();
}

void OptionsDialog::on_tableView_doubleClicked(const QModelIndex &index)
{
    QString ros = QInputDialog::getText(this, "Dialog", "Entyer text");
    Q_UNUSED(index)
}

//void OptionsDialog::on_comboBox_currentRowChanged(QString itemChanged)
//{
//    mAddForm = new AddFieldForm;
//    mAddForm->show();

//    if (itemChanged == "ADD FIELD")
//    {
//        mAddForm->show();

//    }
//    qDebug() << itemChanged;
//}

void OptionsDialog::on_comboBox_currentRowChanged(QString itemChanged)
{
    mAddFieldForm = new AddFieldForm;
    if (itemChanged == "ADD FIELD")
    {
        mAddFieldForm->setWindowFlags(Qt::Tool | Qt::Dialog);
        mAddFieldForm->show();

    }
    qDebug() << itemChanged;
}


void OptionsDialog::setName(QString newName)
{
    ui->lineEdit->setText(newName);
}

bool OptionsDialog::eventFilter(QObject *watched, QEvent *event)
{
    if(watched == ui->tableView->viewport() && event->type() == QEvent::MouseButtonDblClick){
//        QString ros = QInputDialog::getText(this, "Dialog", "Enter text"); qDebug() << ros;
        mEdit = new EditForm();
        mEdit->setWindowFlags(Qt::Tool | Qt::Dialog);
        mEdit->show();
    } return QDialog::eventFilter(watched, event);

}



void OptionsDialog::on_addBtn_clicked()
{
    for(int i=0; i< ui->tableWidget->rowCount(); i++)
        {
            QString viewFieldName = ui->tableWidget->item(i, 0)->text();
            mModel->setHeaderData(i, Qt::Horizontal, viewFieldName);
            mModel->submitAll();
        }
}
